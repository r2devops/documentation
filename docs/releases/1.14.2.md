---
title: 1.14.2 release
description: > 
    R2Devops 1.14.2 released with New Analysis Dashboard. Fix your CI/CD Supply Chain, stay informed with real-time CI/CD tracking, detect CVEs and ensure pipelines compliance.
    
date: 2023-08-04
---

<p hidden>#more</p>

# **R2Devops 1.14.2 release**

**R2Devops 1.14.2 released with New Analysis Dashboard** 

!!! info "Docker images versions"
    - Backend: `v1.14.2`
    - Frontend: `v1.9.1` 


**Fix your CI/CD Supply Chain, stay informed with real-time CI/CD tracking, detect CVEs and ensure pipelines compliance.**

!!! info
    To get the most complete experience of R2Devops Dashboard, it's recommended to use compliant templates located in Innersource and Opensource R2Devops Marketplace

## Overview

Get an overview of all CI/CD configuration across your organization.

Quickly identify security issues, outdated and unmaintainable resources

![Gif of overview section of the Analysis Dashboard](../../images/dashboard_overview.gif)

## Maintainability

Identify unmaintainable resources, outdated jobs and quickly fix them.

![Gif of maintainability section of the Analysis Dashboard](../../images/dashboard_maintainability.gif)


## Compliance

Dynamically add pipeline label definitions to test projects labels composition.

![Gif of compliance section of the Analysis Dashboard](../../images/dashboard_compliance.gif)

## Security

*The Security section is separated in multiple tabs, Variables / Secrets / Dependencies / Containers / Scripts*

### Variables

Quickly identify unmasked or unprotected variables use inside your CI/CD supply chain

![Gif of variables security section of the Analysis Dashboard](../../images/dashboard_variables.gif)

### Secrets

Quickly identify secrets leaks inside your CI/CD supply chain

![Gif of secrets security section of the Analysis Dashboard](../../images/dashboard_secrets.gif)

### Minor updates

- Migrate Frontend to Next.js: Better performance and improved SEO capabilities
- New Roadmap page: https://r2devops.io/roadmap

### Conclusion

R2Devops version 1.14.2 brings a totally new game changing way to maintain CI/CD across your organization, another step forward to make CI/CD accessible to everyone.