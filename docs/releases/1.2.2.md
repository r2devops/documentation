---
 
title: 1.2.2 release
description: > 
    Let's have a look to all improvements made in R2Devops in November 2022!
date: 2022-12-01
---

<p hidden>#more</p>

# **R2Devops 1.2.2 release**

**I'm sure you can't wait to see what we did in November 📅**   
**Without further ado, check out the progress of R2Devops in this article 👇**

---
## Major improvements

### New graphical interface for the pipeline generator page

**⚠️this feature is currently disabled on R2Devops⚠️**

The main improvement of November: You can now display or generate a pipeline, visualize it in a graphical view and display GitLab CI/CD config linter to finally push it to a new commit in GitLab!

![Picture of the new graphical interface of pipeline playground](../../images/Graphical_Interface.gif)

Come and test it for free on your GitLab projects from your [R2Devops account](https://r2devops.io/u/signin) 🔥

## **Minor improvements**

- We now check the GitLab Personnel Access Token expiration date, to warn user about it when it will expire in less than 10 days. If the token is expired, redirect to [`Account`](https://r2devops.io/dashboard/account) page when trying to access [`Private Catalog`](https://r2devops.io/dashboard/catalog) and [`Import`](https://r2devops.io/dashboard/import) pages.

![Gif of the token expiration modale](../../images/TokenGitlab.gif)

- We add an admonition in the import page to explain how the job structure must be, in order to be imported into R2Devops.

![Picture of the admonition un the import job page](../../images/Admonition.png)

- Add a Changelog tab in the job page to visualize it 👇

![Gif of the new Changelog tab in the job page](../../images/Changelog.gif)

- Improve import reports design and implement version file reporting.

### **Job’ updates**

- [code_spell](https://r2devops.io/_/r2devops-bot/codespell)
- [trivy_dependency](https://r2devops.io/_/r2devops-bot/trivy_dependency)
- [mega_linter](https://r2devops.io/jobs/tests/mega_linter/)
- [trivy_image](https://r2devops.io/_/r2devops-bot/trivy_image)
- [openapi](https://r2devops.io/_/r2devops-bot/openapi)
- [mkdocs](https://r2devops.io/_/r2devops-bot/mkdocs)
- [gofmt](https://r2devops.io/_/r2devops-bot/gofmt)
- [gitleaks](https://r2devops.io/_/r2devops-bot/gitleaks)
- [golint](https://r2devops.io/_/r2devops-bot/golint)
- [go_unit_test](https://r2devops.io/_/r2devops-bot/go_unit_test)
- [job_release](https://r2devops.io/_/r2devops-bot/job_release)