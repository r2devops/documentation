const marginStruct = [0, 0];
var tab = false;

function loadListeners() {
    /*
     * This following screen is to expand md-content when we
     * select the .gitlab-ci.yml tab, and retract it to go
     * back to default state
     */

    let sidebar = document.getElementsByClassName('md-sidebar--secondary')[0];
    let content = document.getElementsByClassName('md-content')[0];
    let tabbed_1 = document.getElementById("__tabbed_1_1");
    let tabbed_2 = document.getElementById("__tabbed_1_2");

    function resizeFirst() {
        // Click on `.gitlab-ci.yml`
        marginStruct[0] = screen.width;

        // Handling different screen resolutions
        if (screen.width <= 768) {
            tab = true;
        }
        else {
            if (screen.width <= 1220)
                marginStruct[1] = 0;
            else marginStruct[1] = 1;

            sidebar.style.display = "none"
            content.style.maxWidth = `calc(100% - 12.1rem * ${marginStruct[1]})`
            tab = true;
        }
    }

    function resizeSecond() {
        // Click on `Documentation`
        marginStruct[0] = screen.width;

        // Handling different screen resolutions
        if (screen.width <= 768) {
            tab = false;
        }
        else {
            if (screen.width <= 1220)
                marginStruct[1] = 1;
            else marginStruct[1] = 2

            sidebar.style.display = "block"
            content.style.maxWidth = `calc(100% - 12.1rem * ${marginStruct[1]})`
            tab = false;
        }
    }

    function resizeListener() {
        // In case the user doesn't event click on .gitlab-ci.yml
        if (marginStruct[0] === 0)
            return;

        if (tab && screen.width <= 1220 && marginStruct[0] > 1220
            || screen.width <= 768 && marginStruct[0] > 768
            || tab && screen.width > 768 && marginStruct[0] <= 768) {
            sidebar.style.display = "none"
            marginStruct[1] = 0;
        }
        else if (!tab && screen.width <= 1220 && marginStruct[0] > 1220) {
            sidebar.style.display = "block"
            marginStruct[1] = 1;
        }
        else if (tab && screen.width > 1220 && marginStruct[0] <= 1220) {
            sidebar.style.display = "none"
            marginStruct[1] = 1;
        }
        else if (!tab && screen.width > 1220 && marginStruct[0] <= 1220) {
            sidebar.style.display = "block"
            marginStruct[1] = 2;
        }
        else if (!tab && screen.width > 768 && marginStruct[0] <= 768) {
            sidebar.style.display = "block"
            marginStruct[1] = 1;
        }

        marginStruct[0] = screen.width;
        content.style.maxWidth = `calc(100% - 12.1rem * ${marginStruct[1]})`
    }

    tabbed_1.onchange = resizeSecond;
    tabbed_2.onchange = resizeFirst;
    window.onresize = resizeListener;
}

// Add the class "light" or "dark" to the body if the theme is toggle
let toggleBtn = document.getElementsByClassName("md-header__option")[0]

if (document.body.getAttribute("data-md-color-scheme") === "default") {
    document.body.classList.remove("dark")
    document.body.classList.add("light")

} else if (document.body.getAttribute("data-md-color-scheme") === "slate") {
    document.body.classList.remove("light")
    document.body.classList.add("dark")
}

let toggleTheme = () => {
    if (document.body.getAttribute("data-md-color-scheme") === "default") {
        document.body.classList.remove("light")
        document.body.classList.add("dark")
    } else if (document.body.getAttribute("data-md-color-scheme") === "slate") {
        document.body.classList.remove("dark")
        document.body.classList.add("light")
    }
}
toggleBtn.addEventListener("click", toggleTheme)




