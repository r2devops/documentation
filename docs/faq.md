---
title: FAQ
description: Find here all the questions and their answers about R2Devops
---

# Welcome to the FAQ!

Here you'll find the answer to your questions! If the question you are looking for isn't on the FAQ, you can contact our support or open a ticket to add it 👇🏻

<a alt="Use the hub" href="https://discord.r2devops.io?utm_medium=website&utm_source=r2devopsdocumentation&utm_campaign=faq" target="_blank">
    <button class="md-button border-radius-10 md-button-center" >
        On Discord 💬
    </button>
</a>

<a alt="Use the hub" href="https://tally.so/r/w5Edvw" target="_blank">
    <button class="md-button border-radius-10 md-button-center" >
        Open a ticket 🎟
    </button>
</a>

## R2Devops platform

### Organizations & private resources

??? info "How can I add a new organization in R2Devops?"

     We get your organizations directly from GitLab, so all you have to do is create a new organization in GitLab!

     To add a new organization in GitLab, follow [this tutorial](https://docs.gitlab.com/ee/topics/set_up_organization.html)!


??? info "What is a private template?"

     A private template is sourced from a private repository on GitLab scoped to your current organization, where are securely store all the organization CI/CD resources.


??? info "Who can access my organization private template?"

     Like on GitLab, only members of the repository where the template is sourced can access it!


<!-- ## Access token

!!! info
    Personal access tokens allow you to use R2Devops features that interact with your repositories on GitLab.


### 1. 🦊 Create a GitLab PAT

Go to the [Personal Access Tokens](https://gitlab.com/-/profile/personal_access_tokens){target=_blank} page in GitLab (more information in [GitLab documentation](https://docs.gitlab.com/ee/user/profile/personal_access_tokens.html){target=_blank}).

In the first section (named `Personal Access Token`), create a new `personal access token` with following parameters:

- Token name: `r2devops.io` (you can choose any name you want)
- Expiration date: this is not mandatory. If you set an expiration date, you will have to re-create a token and add it in R2Devops at each expiration.
- Select scopes: select only `api`

Example:

![PAT form example](../images/pat_form.png)

### 2. 🌌 Add it in R2Devops

Once it has been created, follow this steps.

  1. Copy the token
  2. [Log in R2Devops](https://r2devops.io/u/signin){target=_blank}
  3. Paste the token in the field
  4. Save it

That's it, you can now use R2Devops features that interact with your repositories on GitLab!

### 3. 🚀 How it's used ?

#### Read

  - Get your projects' list
  - Get your access level in group and projects
  - Get your projects repository if you explicitly import it as a catalog template source

#### Write

  - Write in your CI configuration file if you explicitely commit through the R2Devops [CI/CD Editor](https://r2devops.io/dashboard/projects){target=_blank}
  - Write project badges when you have edited a pipeline on the [CI/CD Editor](https://r2devops.io/dashboard/projects){target=_blank} or created an organization template catalog on the [Sources](https://r2devops.io/dashboard/sources){target=_blank} page -->

## Other

??? info "What will be the next features developed in R2Devops?"

     You can take a look to the [roadmap](https://r2devops.io/roadmap){target=_blank} to see the next feature we will develop!

??? info "Will R2Devops always be free?"

     The library of open source templates (the SaaS Marketplace) will always be available without license, and will stay free!

## R2Devops Hub

### Use a template

??? info "What are R2Devops' standards of quality and security for official templates?"

    When templates have the label "Official", it means they respect standards of quality and safety defined by R2Devops' team.
    Regarding the quality, we ensure:

    - Fixed tag for docker image and any external tool used inside the template, so they don't break the templates if they change.
    - Resources with license compatible with the template license, so anyone can use it.
    - Artifacts and logs production, in order to facilitate the comprehension of the template's results.
    - Simple customization of the templates, thanks to variables.
    - The structure of the template is properly defined, in order to build a clear documentation.

    Regarding the security, take a look at our CI pipeline:

    ![R2Devops' CI pipeline](../images/CI_pipeline.png)

??? info "What is a plug and play template?"

    We described as Plug and Play the templates that don't need configuration in order to work in your pipeline.

    It means you can add the include link of the template in your pipeline and directly run it. And it will work ✨


??? info "How do I include a template in my pipeline?"

    In order to include a template in your pipeline, you need to add the template include project, ref and file in your `.gitlab-ci.yml` file.

    But first, you need to precise the stage in which your template is supposed to work!

??? info "Why should I use an official template?"

     Official templates are checked by R2Devops' team and respect certain standard regarding security and quality. Plus, you can be sure those templates won't be deleted from the hub, and that your pipeline won't broke due to a missing content.


??? info "How can I import a template to the hub ?"

    They are 2 ways to import a template into R2Devops. You can:

    🔥 [Create your own marketplace](/marketplace/manage-templates/#create-a-catalog)

    ❤️ [Contribute on the official repository](/official-templates/contribute)

??? info "Why my contribution wasn't validated yet?"

      Adding your template into the official R2Devops repository will require more time than linking your template.

      Why?

      Because our team personally review your template and ensure it fits our safety and quality requirements!
      Once the first review is done, they'll let you know your work was perfect, and your template is added or if some adjustments are required.

## Labels

??? info "What are the blue labels?"

    Blue labels give information about the origin of the templates.

    In R2Devops, blue labels look like this:

    ![Blue label example](../images/label-official.png){width=20%}

    A template can be added in the hub by someone working for **R2Devops** or by someone from **the community**. If you [import a template](/marketplace/manage-templates), your template will automatically get the label *community*. If you [contribute following R2Devops guidelines](/official-templates/contribute), your template will get the label *Official*!

??? info "What means the label *Official*?"

    ![Blue label Official](../images/label-official.png){width=20%}

    If a template carries the "Official" label, it indicates that it meets [certain standards for quality and security](#use-a-template).

??? info "What means the label *Community*?"

    ![Blue label community](../images/label-community.png){width=20%}

    The label Community means that the template was added by someone from the community. We don't know which quality standards this template follow, so we can't ensure it's safety.

??? info "What are the green labels?"

    Green labels give technical information about a template.

    In R2Devops, green labels look like this:

    ![Green label example](../images/label-green.png){width=20%}


??? info "I can't find the label I want to use"

    If you can't find the label you want to use, you can [open a ticket](https://tally.so/r/w5Edvw) and ask our team to create a new label!
