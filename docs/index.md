---
template: home.html
title: R2Devops documentation
description: Protect your Software Supply Chain with R2Devops! Stay informed with real-time CI/CD tracking, detect CVEs and ensure pipelines compliance
---

<h1 class="flex gap-2" style="margin-bottom:0px!important">
    <img style="width: 50px" src="images/logo-documentation.svg"/>
    Welcome to <p class="default_color">R2Devops</p> Documentation!
</h1>

## ⚡ Get started

<div class="tx-card-container gap-4">
    <a class="w-full" alt="To self-managed R2Devops platform documentation" href="./self-managed/">
        <button class="w-full md-button border-radius-10 md-button" >
            ⚙️  Install your R2Devops instance
        </button>
    </a>
    <a class="w-full" alt="To dashboard documentation" href="./dashboard/how-it-works">
        <button class="w-full md-button border-radius-10 md-button" >
            📊 Configure and use your CI/CD dashboard
        </button>
    </a>
    <a class="w-full" alt="To manage templates documentation" href="./marketplace/manage-templates">
        <button class="w-full md-button border-radius-10 md-button" >
            💼 Create and use your marketplace templates
        </button>
    </a>
    <a class="w-full" alt="To ambassador program documentation" href="./ambassador/">
        <button class="w-full md-button border-radius-10 md-button" >
            🦸 Join our ambassador program
        </button>
    </a>
</div>

## 💬 Community

!!! heart "Community"
    We love talking with our contributors and users! Join our
    [Discord community :fontawesome-brands-discord:](https://discord.r2devops.io/?utm_medium=website&utm_source=r2devopsdocumentation?utm_campaign=addajob)

## 🙋 Support

- <a alt="Join R2Devops.io Discord" href="https://discord.r2devops.io?utm_medium=website&utm_source=r2devopsdocumentation&utm_campaign=homepage" target="_blank">Join us on Discord</a>
- <a alt="Open a ticket" href="https://tally.so/r/w5Edvw" target="_blank">Open a ticket</a>
