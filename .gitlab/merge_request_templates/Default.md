## Definition of Done

📢 In order to ensure a safe :lock: hub, make sure to complete all the tasks related to the type of work you are doing before merging

*Your issue field isn't here ? Feel free to customize your merge request accordingly!*

### Field: Update the documentation

* [ ] Check the documentation result is rendered properly in pipeline `MkDocs` artifact
* [ ] Check if your job follows our [guidelines](https://docs.r2devops.io/official-templates/contribute#guidelines-required) and [best practices](https://docs.r2devops.io/official-templates/contribute#best-practices-optional)
* [ ] Ensure pipeline doesn't fail
* [ ] Update the dictionary if the job `spell_check` fails, and it's not a typo  
* [ ] Fix the broken links if any is prompted by the `links_checker` job

## When I'm done

You are already done :100: ? Well, now put in review one of the R2Devops **team member**, so we can review what you've done so far! :rocket: 

*If you are updating or adding a job, we would be grateful if you could comment in your merge request a direct link to a pipeline with the working job*
